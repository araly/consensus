# Consensus

## Installation

You need to have ![Godot](https://godotengine.org/) installed, this branch is a godot project.

**Godot Install**

For Linux MacOS and Windows, you can also find ![Godot on steam](https://store.steampowered.com/app/404790/Godot_Engine/) which might be the easiest if you don't mind needing steam.

- Linux  
Godot is probably on repositories, for example:
    - AUR (Arch / Manjaro): ![godot](https://aur.archlinux.org/packages/godot/)
    
But to be fair, Godot doesn't need an installation, and can just live in its own folder, so you can just download it from the ![download page](https://godotengine.org/download/linux) and create a symbolic link in your path to it, or have an app linking to it from the desktop.

- MacOS

Same for linux I think, you can find Godot on its ![download page](https://godotengine.org/download/osx) and add it to your Applications folder.

Or you can install it with homebrew:  
`$ brew cask install godot`

- Windows

Again same idea, you can find Godot in its ![download page](https://godotengine.org/download/windows) and create a shortcut to have on your desktop or whatever you fancy.

You can also use Scoop:  
`
scoop bucket add extras
scoop install godot
`

or chocolatey:  
`choco install godot-mono**

**Adding consensus as a project in Godot**

Once you have Godot installed, you can fork or clone the project:  
`git clone https://gitlab.com/araly/consensus.git`

Then add it to Godot: 
- launch Godot
- once in the project list, click on *Import*
- head to wherever you cloned the project
- click on *Import & Edit*

Then press *F5* to run the project.

## Releases

You can find releases ![here](https://mega.nz/#F!wgx3Da6A!kL7KI4IxIkY2Z1MG5TLr_A) (mega.nz link) for linux, windows and web. Let me know if you would want another type of release, as Godot can provide more than just those.
