extends Node

var label
var points = 0.0
var confidence = 0.0
var votes = []

func _ready():
	pass

func init(label):
	self.label = label

func calculate_confidence():
	confidence = 0.0
	if votes != []:
		for vote in votes:
			confidence += vote.confidence
	votes = [] # TODO pay attention that votes are forgotten everywhere

func add_vote(vote_to_add):
	votes.append(vote_to_add)

func to_string():
	var to_return = str(label, ": ", int(points)) # displaying as int
	if votes != []:
		for vote in votes:
			to_return = str(to_return, "\n- ", vote.to_string())
	return to_return;
