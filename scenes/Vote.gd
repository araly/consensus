extends Node

var user
var confidence

func _ready():
	pass

func init(user, confidence):
	self.user = user
	self.confidence = confidence

func to_string():
	return str(user, ": ", confidence, "c")