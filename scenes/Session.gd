extends Node

var candidates
var total_points_to_reatribute = 0.25
var winning_threshold = 100.0

# TODO init (constructor)

func _ready():
	pass

func init(candidates):
	self.candidates = candidates

func calculate_points():
	if candidates.size() > 0:
		var points_to_reatribute = 0.0
		
		# calculate total confidence
		var total_confidence = 0.0
		for candidate in candidates:
			total_confidence += candidate.confidence
		# we should have a minimum confidence. if not, just abort
		if total_confidence <= 0.0:
			return {error = "total_confidence was not positive", total_confidence = total_confidence, from = "Session.calculate_points()"}
		
		# each candidate pays a fee with their points so that we have total_points_to_reatribute = points_to_reatribute
		var fee = 100.0 * total_points_to_reatribute / candidates.size()
		for candidate in candidates:
			points_to_reatribute += fee
			candidate.points -= fee
		
		# reatribute the points proportionally to their confidence
		var minimum_points = 0.0
		for candidate in candidates:
			candidate.points += float(candidate.confidence) / total_confidence * points_to_reatribute
			if candidate.points < minimum_points:
				minimum_points = candidate.points

		# When candidates have negative points, we just bring them back to 0, cuz that's just sad, and to give them a chance to climb up again
		for candidate in candidates:
			if candidate.points < 0.0:
				candidate.points = 0.0

func play_turn():
	calculate_confidences()
	var result = calculate_points()
	reset_confidence()
	if result != null:
		return result
	return seek_for_a_winner()

func seek_for_a_winner():
	for candidate in candidates:
		if candidate.points >= winning_threshold:
			return candidate

func calculate_confidences():
	for candidate in candidates:
		candidate.calculate_confidence()

func reset_confidence(): # to throw away when votes get introduced
	for candidate in candidates:
		candidate.confidence = 0

func to_string():
	var to_return = ""
	for candidate in candidates:
		to_return = str(to_return, "\n", candidate.to_string())
	return to_return

