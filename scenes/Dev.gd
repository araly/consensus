# instead of stopping at 100 points, we stop at a certain percentage of difference between the leading and the second place
# actually I think making it a percentage is what breaks it. if we make it relative, it turns relative. if we fix it, it allows candidates to pass the threshold, instead of seeing it grow as they grow, at an unnatainable pace

extends Node

var Candidate = preload("res://scenes/Candidate.tscn")
var Session = preload("res://scenes/Session.tscn")
var Vote = preload("res://scenes/Vote.tscn")

var c1 = Candidate.instance()
var c2 = Candidate.instance()
var c3 = Candidate.instance()

var session = Session.instance()

func _ready():
	randomize()
	test()
	update_display("intialization\npress buttons to add confidence\nget 100p to win")

func _process(delta):
	enable_disable_NextButton()
	pass

func test():
	test_initialization()

func test_initialization():
	c1.init("c1")
	c2.init("c2")
	c3.init("c3")
	session.init([c1, c2, c3])

func enable_disable_NextButton():
	var NextButton_disabled = true
	for candidate in session.candidates:
		if candidate.votes != []:
			NextButton_disabled = false
	get_node("NextButton").disabled = NextButton_disabled

func update_display(message):
	get_node("DisplayLabel").text = str(session.to_string(), "\n", message)

func _on_NextButton_pressed():
	var result = session.play_turn()
	if typeof(result) == TYPE_DICTIONARY && result.has("error"):
		update_display(result)
	elif typeof(result) == TYPE_OBJECT:
		update_display(str("**", result.label, " won the session**"))
	else:
		update_display("")

func _on_Candidate1Button_pressed():
	var new_vote = Vote.instance()
	new_vote.init(str("hector", randf()), 10)
	c1.add_vote(new_vote)
	update_display("")

func _on_Candidate2Button_pressed():
	var new_vote = Vote.instance()
	new_vote.init(str("hector", randf()), 10)
	c2.add_vote(new_vote)
	update_display("")

func _on_Candidate3Button_pressed():
	var new_vote = Vote.instance()
	new_vote.init(str("hector", randf()), 10)
	c3.add_vote(new_vote)
	update_display("")
